﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float maxSpeed = 5f;
    public float rotSpeed = 180f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      
        Vector3 pos = transform.position;

        Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        // pos.z += Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;

        z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;

        rot = Quaternion.Euler(0, 0, z );

        transform.rotation = rot;

        pos += rot * velocity;

        if(pos.y > Camera.main.orthographicSize)
        {
            pos.y = Camera.main.orthographicSize;
        }


        //pos.z += Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.position = pos;

        //var speed = 1.0f;

        //var move = Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        //transform.position += move * speed * Time.deltaTime;

        //transform.Translate(Vector3.forward * Time.deltaTime * Input.GetAxis("Vertical") * speed);
        ////Moves Left and right along x Axis                               //Left/Right
        //transform.Translate(Vector3.right * Time.deltaTime * Input.GetAxis("Horizontal") * speed);
    }
}
